import Vue from 'vue';
import Router from 'vue-router';
import DashboardLayout from '@/layout/DashboardLayout';
import AuthLayout from '@/layout/AuthLayout';
Vue.use(Router);
import store from "./store"

const routes = [
    {
        path: '/dashboard',
        redirect: 'dashboard',
        component: DashboardLayout,
        children: [
            {
                path: '/dashboarda',
                name: 'dashboarda',
                // route level code-splitting
                // this generates a separate chunk (about.[hash].js) for this route
                // which is lazy-loaded when the route is visited.
                component: () =>
                    import(
                        './views/Dashboarda.vue'
                    ),
            },
            {
                path: '/dashboard',
                name: 'dashboard',
                component: () =>
                    import(
                        './views/Dashboard.vue'
                    ),
            },
            {
                path: '/loans',
                name: 'loans',
                component: () =>
                    import(
                        './views/Dashboard/Loans.vue'
                    ),
            },
            {
                path: '/investments',
                name: 'investments',
                component: () =>
                    import(
                        './views/Dashboard/Investments.vue'
                    ),
            },
            {
                path: '/profile',
                name: 'profile',
                component: () =>
                    import(
                        './views/Dashboard/UserProfile.vue'
                    ),
            },
            {
                path: '/icons',
                name: 'icons',
                component: () =>
                    import(
                        './views/Icons.vue'
                    ),
            },

            {
                path: '/maps',
                name: 'maps',
                component: () =>
                    import(
                        './views/Maps.vue'
                    ),
            },
            {
                path: '/tables',
                name: 'tables',
                component: () =>
                    import(
                        './views/Tables.vue'
                    ),
            },
        ],
    },
    {
        path: '/',
        redirect: 'login',
        component: AuthLayout,
        children: [
            {
                path: '/login',
                name: 'login',
                component: () =>
                    import(
                        './views/Login.vue'
                    ),
            },
            {
                path: '/register',
                name: 'register',
                component: () =>
                    import(
                        './views/Register.vue'
                    ),
            },
            {
                path: '/profile/information',
                name: 'personal_details',
                component: () =>
                    import(
                        './views/auth/Profile.vue'
                    ),
                beforeEnter(to, from, next) {
                    if (!store.state.registration.hasOwnProperty("profile")) return next("/register");
                    next();
                },
            },
            {
                path: '/profile/nok',
                name: 'next_of_kin',
                component: () =>
                    import(
                        './views/auth/Nok.vue'
                    ),
                beforeEnter(to, from, next) {
                    if (!store.state.registration.hasOwnProperty("home_address")) 
                        return next("/profile/information");
                    next();
                },
            },
            {
                path: '/profile/bank',
                name: 'bank_details',
                component: () =>
                    import(
                        './views/auth/Bank.vue'
                    ),
                beforeEnter(to, from, next) {
                    if (!store.state.registration.hasOwnProperty("next_of_kin")) 
                        return next("/profile/nok");
                    next();
                },
            },
            {
                path: '/profile/employment',
                name: 'employment_details',
                component: () =>
                    import('./views/auth/Employment.vue'),
                beforeEnter(to, from, next) {
                    if (!store.state.registration.account.account_no) 
                        return next("/profile/bank");
                    next();
                },
            },
            {
                path: '/create-account',
                name: 'create_account',
                component: () =>
                    import('./views/auth/Create.vue'),
                beforeEnter(to, from, next) {
                    if (!store.state.registration.hasOwnProperty("work")) 
                        return next("/profile/nok");
                    next();
                },
            },
        ],
    },
];

const authRoutes = routes[0].children.map(key => key.name)

const router = new Router({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes
});

router.beforeEach((to, from, next) => {
    let user = JSON.parse(sessionStorage.getItem('user'));
    let loan_settings = JSON.parse(localStorage.getItem('loan_settings'));
    if (loan_settings) store.commit('set', { loan_settings });

    if (authRoutes.includes(to.name) && !user) return next('/login');

    if (user) store.commit('set', { user });
    next();
});

export default router
