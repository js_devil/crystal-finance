import Vue from "vue";
import Vuex from "vuex";

import reg from "./plugins/new"

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        loader: false,
        loadingText: "",
        user: {},
        loan_application: {},
        updatedProfile: {},
        registration: {},
        transactions: [],
        portfolio: [],
        darkMode: false,
    },
    mutations: {
        set(state, data) {
            for (let key of Object.keys(data)) state[key] = data[key];
        },
        setRegister(state, data) {
            for (let key of Object.keys(data))
                state.registration[key] = state.registration.hasOwnProperty(key)
                    ? { ...state.registration[key], ...data[key] }
                    : data[key];
            console.log(state.registration)
        },
        setLoanApp(state, data) {
            for (let key of Object.keys(data))
                state.loan_application[key] = state.loan_application.hasOwnProperty(key)
                    ? { ...state.loan_application[key], ...data[key] }
                    : data[key];
            // console.log(state.loan_application);
        },
        setOthers(state, data) {
            for (let key of Object.keys(data))
                state.loan_application[key] = data[key];
            console.log(state.loan_application);
        },
        updateProfile(state, data) {
            for (let key of Object.keys(data)) {
                state.updatedProfile[key] = state.updatedProfile.hasOwnProperty(key)
                    ? { ...state.updatedProfile[key], ...data[key] }
                    : data[key];
                console.log(state.updatedProfile);
            }
        },
    },
    actions: {},
    modules: {},
});
