import axios from "axios";
import Swal from "sweetalert2";

const imageUrl =
  "https://upload.wikimedia.org/wikipedia/commons/0/03/Forbidden_Symbol_Transparent.svg";

const getOccupations = () =>
    axios({
        url: `${Endpoint}/occupations`,
        method: "get",
        headers: {
            "x-api-key": `RVxAXlhQnhFCbrk92iGBNgMyUyoQdsapYl2mDJZPt5wni9eBvqcNoS8s106axCI4`,
        },
    });
const getDesignations = () =>
    axios({
        url: `${Endpoint}/designations`,
        method: "get",
        headers: {
            "x-api-key": `RVxAXlhQnhFCbrk92iGBNgMyUyoQdsapYl2mDJZPt5wni9eBvqcNoS8s106axCI4`,
        },
    });

const getSectors = () =>
    axios({
        url: `${Endpoint}/work_sectors`,
        method: "get",
        headers: {
            "x-api-key": `RVxAXlhQnhFCbrk92iGBNgMyUyoQdsapYl2mDJZPt5wni9eBvqcNoS8s106axCI4`,
        },
    });

const getEducation = () =>
    axios({
        url: `${Endpoint}/education_levels`,
        method: "get",
        headers: {
            "x-api-key": `RVxAXlhQnhFCbrk92iGBNgMyUyoQdsapYl2mDJZPt5wni9eBvqcNoS8s106axCI4`,
        },
    });

const getBanks = () =>
    axios({
        url: `${Endpoint}/banks`,
        method: "get",
        headers: {
            "x-api-key": `RVxAXlhQnhFCbrk92iGBNgMyUyoQdsapYl2mDJZPt5wni9eBvqcNoS8s106axCI4`,
        },
    });

const getStates = () =>
    axios({
        url: `${Endpoint}/states`,
        method: "get",
        headers: {
            "x-api-key": `RVxAXlhQnhFCbrk92iGBNgMyUyoQdsapYl2mDJZPt5wni9eBvqcNoS8s106axCI4`,
        },
    });

const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
];

import { mapState } from "vuex";

export default {
    data() {
        return {
            publicPath: process.env.BASE_URL,
            nairaSign: "\u20A6",
            offer: false,
            imageUrl,
            today: new Date().toISOString().slice(0, 10),
        };
    },
    computed: {
        ...mapState([
            "user",
            "registration",
            "loading",
            "loan_settings",
            "transactions",
            "portfolio",
        ]),
        userImg() {
            return `https${this.user.data.profile.file_name.split("https")[2]}`
        },
        invest_returns() {
            const percent = this.invest_amount * (this.interest / 100);
            return this.nairaSign + this.formatNumber(this.invest_amount + percent);
        },
    },
    methods: {
    // functions
        async getEligbleAmount(data) {
            this.$store.commit("set", {
                loader: true,
                loadingText: "",
            });

            try {
                const res = await axios({
                    method: "post",
                    url: `${Endpoint}/county/offer`,
                    data,
                    headers,
                });

                const {
                    product_id,
                    tenor,
                } = this.$store.state.loan_application.request;

                const { LOAN_INTEREST: interest } = this.$store.state.products.find(
                    (key) => key.LOAN_PRODUCT_ID === product_id
                );

                let repay = {};
                repay.total_amount = this.getRepayment(
                    Number(res.data.data.max_amount),
                    Number(tenor) || 0,
                    Number(interest)
                );
                repay.amount = repay.total_amount / (tenor || 0);

                await this.$store.commit("set", {
                    loader: false,
                    offer_amount: res.data.data.max_amount || 0,
                    loadingText: "",
                });

                if (!res.data.status) return this.showError("", res.data.message);
                await this.$store.commit("setLoanApp", {
                    request: {
                        amount: res.data.data.max_amount,
                    },
                    repayment: repay,
                });
                this.offer = true;

                Swal.fire({
                    icon: "info",
                    text: res.data.message,
                });
            } catch (err) {
                this.$store.commit("set", {
                    loader: false,
                    loadingText: "",
                });
                this.catchErrors(err);
            }
        },
        getRepayment: (amount, duration, interest) =>
            !amount || !interest || !duration
                ? 0
                : amount + (interest / 100) * amount * duration,
        async getLoanHistory(token) {
            try {
                const res = await axios({
                    method: "post",
                    url: `${Endpoint3}/customer/loans`,
                    headers,
                    data: { token },
                });

                this.$store.commit("set", { loans: res.data.data });
                if (this.$route.name == "Loans") return res.data.data;
            } catch (err) {
                this.catchErrors(err);
            }
        },
        async getBanks() {
            try {
                const res = await axios({
                    url: `${Endpoint}/banks`,
                    method: "get",
                    headers: {
                        "x-api-key": `RVxAXlhQnhFCbrk92iGBNgMyUyoQdsapYl2mDJZPt5wni9eBvqcNoS8s106axCI4`,
                    },
                });
                const banks = (res.data.data || []).sort((a, b) =>
                    a.name > b.name ? 1 : -1
                );

                return banks;
            } catch (err) {}
        },
        async getAccounts(token) {
            if (!token) return;

            try {
                const res = await axios({
                    url: `${Endpoint3}/customer/accounts`,
                    headers,
                    method: "post",
                    data: { token },
                });
                this.accounts = res.data.data;
            } catch (e) {
                this.getAccounts(token);
                this.catchErrors(e);
            }
        },
        async getCards(token) {
            if (!token) return;

            try {
                const res = await axios({
                    url: `${Endpoint3}/customer/cards`,
                    headers,
                    method: "post",
                    data: { token },
                });

                this.cards = res.data.data;
            } catch (e) {
                this.catchErrors(e);
            }
        },
        async investment(url, request) {
            this.$store.commit("set", { loader: true, loadingText: "Generating..." });

            try {
                const res = await axios({
                    url: `${Endpoint3}${
                        !url.includes("transaction") ? "/customer" : ""
                    }/investment/${url}`,
                    method: "post",
                    headers,
                    data: { request, token: this.user.token },
                });

                if (!res.data.status) return this.showError("", res.data.message);

                // close modal
                for (const modal of [
                    "#investheritfund",
                    "#investflexfund",
                    "#investfixfund",
                    "#withdrawalconfirmation",
                ]) {
                    if ($(modal).is(":visible")) {
                        await this.getUserDetails(this.user);
                        $(modal).modal("toggle");

                        await this.investment("portfolio", {});
                        await this.investment("recent/transactions", {});

                        Swal.fire({
                            icon: "success",
                            title: "Your investment request was successful",
                            text: "We will reach out to you soon",
                        });
                    }
                }

                this.$store.commit("set", { loader: false, loadingText: "" });

                // portfolio
                if (url == "portfolio")
                    return this.$store.commit("set", { portfolio: res.data.loans });
                if (url == "recent/transactions")
                    return this.$store.commit("set", {
                        transactions: res.data.chart_account_transactions,
                    });
            } catch (err) {
                this.$store.commit("set", { loader: false, loadingText: "" });
                this.catchErrors(err);
            }
        },
        async getUserDetails(user) {
            if (!user) return;

            try {
                const res = await axios({
                    url: `${Endpoint3}/user/details`,
                    method: "post",
                    headers,
                    data: { token: user.token },
                });

                if (!res.data.status) return this.showError("", res.data.message);
                let user_ = {
                    ...user,
                    loanHistory: res.data.data.loanHistory,
                    data: res.data.data.userData.data,
                    is_individual: res.data.data.userData.is_individual,
                    timeline: res.data.data.timeline,
                    total_investments: res.data.data.total_investments,
                    total_loans: res.data.data.total_loans,
                };
                this.$store.commit("set", {
                    user: user_,
                });
                sessionStorage.setItem("user", JSON.stringify(user_));
            } catch (err) {
                // this.$store.commit("set", { loader: false, loadingText: "" });
                // this.catchErrors(err);
            }
        },

        // resources
        async getResources() {
            try {
                const res = await Promise.all([
                    getOccupations(),
                    getDesignations(),
                    getSectors(),
                    getEducation(),
                ]);

                this.resources = {
                    occupations: res[0].data.data,
                    designations: res[1].data.data,
                    sectors: res[2].data.data,
                    education: res[3].data.data,
                };
            } catch (err) {}
        },

        //
        numberWithCommas: (x) => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        removeCommas: (amount) => amount.replace(/,/g, ""),
        formatNumber: (amount) =>
            amount && parseFloat(amount).toFixed(0).length < 7
                ? parseFloat(parseFloat(amount).toFixed(2)).toLocaleString("en")
                : Math.abs(Number(amount)) >= 1.0e6
                    ? Number(parseFloat(Math.abs(Number(amount)) / 1.0e6).toFixed(2)) +
          " Million"
                    : Math.abs(Number(amount)) >= 1.0e9
                        ? Number(parseFloat(Math.abs(Number(amount)) / 1.0e9).toFixed(2)) +
          " Billion"
                        : Math.abs(Number(amount)) >= 1.0e12
                            ? Number(parseFloat(Math.abs(Number(amount)) / 1.0e12).toFixed(2)) +
          " Trillion"
                            : "0.00",
        datesAreOnSameDay: (first, second) =>
            first.getFullYear() === second.getFullYear() &&
      first.getMonth() === second.getMonth() &&
      first.getDate() === second.getDate(),
        formatTime(date) {
            let hours = date.getHours();
            let minutes = date.getMinutes();
            let ampm = hours >= 12 ? "pm" : "am";
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? "0" + minutes : minutes;
            return `${hours}:${minutes} ${ampm}`;
        },
        formatDate(date) {
            const sameDay = this.datesAreOnSameDay(new Date(date), new Date());
            if (sameDay) return "Today - " + this.formatTime(new Date(date));
            return `${months[new Date(date).getMonth()]} ${new Date(
                date
            ).getDate()}, ${String(new Date(date)).slice(11, 15)}`;
        },
        turnOnDarkMode(self, element) {
            $(".menu-w, .top-bar").removeClass(function(index, className) {
                return (className.match(/(^|\s)color-scheme-\S+/g) || []).join(" ");
            });
            $(".menu-w").removeClass(function(index, className) {
                return (className.match(/(^|\s)color-style-\S+/g) || []).join(" ");
            });
            $(".menu-w")
                .addClass("color-scheme-dark")
                .addClass("color-style-transparent")
                .removeClass("selected-menu-color-light")
                .addClass("selected-menu-color-bright");
            $(".top-bar").addClass("color-scheme-transparent");
            $(element)
                .find(".os-toggler-w")
                .addClass("on");

            $(element)
                .find(".os-icon-eye")
                .addClass("eye-on");

            $(element).find(".os-toggler-pill");
            // .style.backgroundColor = "#ff0000";
            self.$store.commit("set", { darkMode: true });
        },
        turnOffDarkMode(self, element) {
            $(".menu-w")
                .removeClass("color-scheme-dark")
                .addClass("color-scheme-light")
                .removeClass("selected-menu-color-bright")
                .addClass("selected-menu-color-light");
            $(element)
                .find(".os-toggler-w")
                .removeClass("on");
            $(element)
                .find(".os-icon-eye")
                .removeClass("eye-on");
            // this.darkMode = true;
            self.$store.commit("set", { darkMode: false });
        },
        toggleTheme(self) {
            $(".floated-colors-btn").on("click", function() {
                if ($("body").hasClass("color-scheme-dark")) {
                    self.turnOffDarkMode(self, this);
                } else {
                    // turn on dark mode
                    self.turnOnDarkMode(self, this);
                }
                $("body").toggleClass("color-scheme-dark");
                return false;
            });
        },

        logout() {
            Swal.fire({
                title: "Are you sure?",
                text: "You are about to log off!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Yes, Log me out",
            }).then((result) => {
                if (result.value) {
                    sessionStorage.removeItem("user");
                    this.$router.push("/login");
                    Swal.fire("Logged Out!", "See you next time", "success");
                }
            });
        },

        catchErrors({ response }) {
            if (!response)
                return Swal.fire({
                    imageUrl,
                    imageAlt: "forbidden",
                    imageHeight: 150,
                    title: "Network Error!",
                    text: "Please check your internet connection and try again",
                    showConfirmButton: false,
                });

            const { message, error } = response.data;

            Swal.fire({
                imageUrl,
                imageAlt: "forbidden",
                imageHeight: 200,
                title: "Oops!",
                text: message ? message : error,
                // showConfirmButton: false,
            });
        },
        showError(title, text) {
            this.$store.commit("set", { loader: false, loadingText: "" });

            const message = text.includes("secure") ? { html: text } : { text };

            return Swal.fire({
                imageUrl,
                imageAlt: "forbidden",
                imageHeight: 200,
                title: title || "Oops!",
                ...message,
            });
        },
        turnToLower(a) {
            for (let i in a) {
                a = {
                    ...a,
                    [i]: a[i] ? `${a[i]}`.toLowerCase() : null,
                };
            }
            return a;
        },
        // validation
        checkEmptyProperties(obj) {
            let count = 0;
            for (let i in obj) {
                if (!obj[i] || !String(obj[i]).length) count++;
            }
            return count ? false : true;
        },
        status: (accepted, default_, status) =>
            status >= 0 && status <= 2 && accepted == 1
                ? "Approved"
                : status >= 0 && status <= 2 && default_ == 1
                    ? "Default"
                    : status >= 0 && status <= 2
                        ? "Processing"
                        : status == 3
                            ? "Active"
                            : status == 5
                                ? "Closed"
                                : "Rejected",
        pill: (accepted, default_, status) =>
        // status >= 0 && status <= 2 && accepted == 1
        //   ? ""
        //   : status >= 0 && status <= 2 && default_ == 1
        //   ? "black"
        //     :
            status >= 0 && status <= 2
                ? "blue"
                : status == 3
                    ? "green"
                    : status == 5
                        ? "black"
                        : "red",

        // validateName: (value) => /^[a-z A-Z]+$/.test(value) && value.length > 2,
        validateName: (value) => value.length > 2,
        validatePassword: (value) => /^([0-9]|[a-z])+([0-9a-z]+)$/i.test(value),
        validateEmail: (email) => /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email),
        validatePhone: (value) => /^\d+$/.test(value) && value.length === 11,
        validateNumbers: (value) => /^\d+$/.test(value),
        capitalize: (string) => string.charAt(0).toUpperCase() + string.slice(1),
    },
};
